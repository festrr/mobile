import { Component } from '@angular/core';
import { AlertController, ModalController, NavController } from 'ionic-angular';
import { User } from '../../../../../../../api/both/models';

@Component({
  selector: 'festrr-login',
  templateUrl: 'login.html'
})
export class HomePageComponent {
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController) {
  }

  login(authenticate: User) {
    Accounts.createUser(authenticate, (error) => {
      if (error) {
        const alert = this.alertCtrl.create({
          title: 'Alert',
          subTitle: error.reason,
          buttons: ['Dismiss']
        });
        alert.present();
      } else {
        Meteor.call('sendVerificationLink', (response) => {
          if (error) {
            const alert = this.alertCtrl.create({
              title: 'Alert',
              subTitle: error.reason,
              buttons: ['Dismiss']
            });
            alert.present();
          } else {
            const alert = this.alertCtrl.create({
              title: 'Welcome',
              subTitle: 'Here we go!',
              buttons: ['Dismiss']
            });
            alert.present();
          }
        });
      }
    });
  }
}
