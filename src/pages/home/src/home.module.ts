import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { HomePageComponent } from './containers/login/login';

const COMPONENTS = [HomePageComponent, LoginFormComponent];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    IonicModule.forRoot(HomePageComponent),
    NgxErrorsModule
  ],
  entryComponents: [COMPONENTS],
  exports: [COMPONENTS]
})
export class HomePageModule {
}
